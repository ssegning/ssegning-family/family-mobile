import 'package:accountmanager/accountmanager.dart';
import 'package:family_mobile/auth/accounts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_appauth/flutter_appauth.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final account = await getStarterAccount();
  runApp(MyApp(account));
}

class MyApp extends StatelessWidget {
  final Account account;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: "Mifa Account", account: this.account),
    );
  }

  MyApp(this.account);
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.account}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  final Account account;

  @override
  _MyHomePageState createState() => _MyHomePageState(account: this.account);
}

class _MyHomePageState extends State<MyHomePage> {
  Account account;

  _MyHomePageState({this.account});

  Future<void> login() async {
    try {
      final AuthorizationTokenResponse authResponse =
      await appAuth.authorizeAndExchangeCode(
        AuthorizationTokenRequest(
          'mifa-mobile',
          'mifa:/oauthredirect',
          discoveryUrl:
          'https://mifa.ssegning.com/auth/realms/mifa/.well-known/openid-configuration',
          scopes: ['openid', 'profile', 'email', 'offline_access', 'api'],
        ),
      );

      print("Meh1 ${authResponse.tokenAdditionalParameters}");
      print("Meh2 ${authResponse.authorizationAdditionalParameters}");
      print("Meh3 ${authResponse.idToken}");

      /**
          var account = new Account('selast', kAccountType);
          if (await AccountManager.addAccount(account)) {
          var token = new AccessToken('Bearer', 'Blah-Blah code');
          await AccountManager.setAccessToken(account, token);
          accounts = await AccountManager.getAccounts();
          for (Account account in accounts) {
          if (account.accountType == kAccountType) {
          token = await AccountManager.getAccessToken(account, token.tokenType);
          name = account.name + ' - ' + token.token;
          break;
          }
          }
          }
       */
    } catch (e, s) {
      print("Error is $e");
      print("Error is also $s");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Account is ${account?.name} - ${account?.accountType}',
            ),
            Text(
              '-> ${account != null ? account : ''}',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: login,
        tooltip: 'Login',
        child: Icon(Icons.login),
      ),
    );
  }
}
