import 'dart:async';

import 'package:accountmanager/accountmanager.dart';
import 'package:flutter_appauth/flutter_appauth.dart';
import 'package:permission_handler/permission_handler.dart';

final String kAccountType = "com.ssegning.family.account";
final FlutterAppAuth appAuth = FlutterAppAuth();

FutureOr<Account> getStarterAccount() async {
  final contactPermissionStatus = await Permission.contacts.request();
  if (!contactPermissionStatus.isGranted) {
    return null;
  } else {
    try {
      var accounts = await AccountManager.getAccounts();
      for (Account account in accounts) {
        if (account.accountType == kAccountType) {
          return account;
        }
      }
    } catch (e, s) {
      print("Error is $e");
      print("Error is also $s");
    }
  }
}
