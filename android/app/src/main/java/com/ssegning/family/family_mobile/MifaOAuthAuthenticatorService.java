package com.ssegning.family.family_mobile;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class MifaOAuthAuthenticatorService extends Service {

    private MifaOAuth2Authenticator authenticator;

    @Override
    public void onCreate() {
        super.onCreate();
        authenticator = new MifaOAuth2Authenticator(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();
    }
}
